from datetime import datetime
from pydantic import BaseModel, Field, EmailStr, constr


class UserBaseSchema(BaseModel):
    name: str = Field(
        ...,
        max_length=100,
        min_length=2,
        example="Fernando"
    )
    age: int = Field(
        gt=0,
        lt=116,
        example=28
    )
    email: EmailStr
    
    created_at: datetime = Field(
        default = datetime.now(),
    )
    updated_at: datetime | None = None

    class Config:
        orm_mode = True


class CreateUserSchema(UserBaseSchema):
    password: constr(min_length=8)
    passwordConfirm: str
    verified: bool = False


class LoginUserSchema(BaseModel):
    email: EmailStr
    password: constr(min_length=8) = Field(
        ...,
        example="rockmax28"
    )


class UserResponseSchema(UserBaseSchema):
    id: str
    pass


class UserResponse(BaseModel):
    status: str
    user: UserResponseSchema
