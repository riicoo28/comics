from serializers.characters import charactersListEntity
from config.config import settings
from utils import utils
import requests

def comicEntity(comic) -> dict:
    characters = None
    if "characters" in comic and comic['characters']:
        if 'collectionURI' in comic['characters']:
            url_marvel = comic["characters"]['collectionURI']
            hash_created = utils.create_hash()
            data = {
                "ts":hash_created['timestamp'],
                "apikey":settings.MARVEL_PUBLIC_KEY,
                "hash":hash_created['md5'],
            }
            response = requests.get(url_marvel, params=data)
            characters = charactersListEntity(response.json()["data"]["results"])
        else:
            characters = comic['characters']

    return {
        "id": str(comic["id"]),
        "title": str(comic["title"]),
        "image": str(comic["thumbnail"]["path"] + '.' + comic["thumbnail"]["extension"])\
            if 'thumbnail' in comic else comic['image'],
        "onSaleDate": str(comic["dates"][0]["date"]) if "dates" in comic else comic['onSaleDate'],
        "characters": characters
    }

def comicsListEntity(comics) -> list:
    return [comicEntity(comic) for comic in comics]