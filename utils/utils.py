import hashlib
from passlib.context import CryptContext
from datetime import datetime
from config.config import settings

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def hash_password(password: str):
    return pwd_context.hash(password)


def verify_password(password: str, hashed_password: str):
    return pwd_context.verify(password, hashed_password)

def create_hash():
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    to_md5 = str(timestamp) + settings.MARVEL_PRIVATE_KEY + settings.MARVEL_PUBLIC_KEY
    md5 = hashlib.md5(bytes(to_md5, 'utf-8')) .hexdigest()
    return {
        "timestamp":timestamp,
        "md5":md5,
    }