from datetime import datetime
from pydantic import BaseModel, Field, EmailStr, constr

from schemas.users import UserBaseSchema
from schemas.comics import ComicBaseSchema

class LayawayBaseSchema(BaseModel):
    user:UserBaseSchema
    comic:ComicBaseSchema
    created_at: datetime = Field(
        default = datetime.now(),
    )
    updated_at: datetime | None = None

    class Config:
        orm_mode = True