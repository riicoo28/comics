from datetime import datetime
from pydantic import BaseModel, Field, EmailStr, constr

class CharacterBaseSchema(BaseModel):
    id: str
    name: str
    image: str
    appearances: int