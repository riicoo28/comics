from datetime import datetime, timedelta
from bson.objectid import ObjectId
from fastapi import APIRouter, Response, status, Depends, HTTPException, Body

import requests
from config import oauth2
from config.database import User
from serializers.users import userEntity, userResponseEntity
from schemas import users
from utils import utils
from config.oauth2 import AuthJWT
from config.config import settings


router = APIRouter(
    prefix="/auth",
    tags=["AUTH"],
    responses={404: {"description": "Not found"}},
)
ACCESS_TOKEN_EXPIRES_IN = settings.ACCESS_TOKEN_EXPIRES_IN
REFRESH_TOKEN_EXPIRES_IN = settings.REFRESH_TOKEN_EXPIRES_IN


@router.post(
    '/register',
    status_code=status.HTTP_201_CREATED,
    response_model=users.UserResponse
)
def create_user(payload: users.CreateUserSchema = Body()):
    """Create user,

    Description:
    
        Allows create a new user

    Args:

        -name: str: Name user
        -age: int: Age user
        -email:str "user@example.com" user email
        -password:str "stringst" Password to access
        -passwordConfirm:str "string" Password confirmation

    Returns:
        
        A json with basic user information:

            {
            "status": "success",
            "user": {
                -name: str: Name user
                -age: 28: int
                -email: str
                -created_at": datetime,
                -updated_at": datetime,
                -id": str
            }
            }

    """
    # Check if user already exist
    user = User.find_one({'email': payload.email.lower()})
    if user:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail='Account already exist'
        )
    # Compare password and passwordConfirm
    if payload.password != payload.passwordConfirm:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail='Passwords do not match'
        )
    #  Hash the password
    payload.password = utils.hash_password(payload.password)
    del payload.passwordConfirm
    payload.verified = True
    payload.email = payload.email.lower()
    payload.created_at = datetime.utcnow()
    payload.updated_at = payload.created_at
    result = User.insert_one(payload.dict())
    new_user = userResponseEntity(User.find_one({'_id': result.inserted_id}))
    return {"status": "success", "user": new_user}


@router.post('/login')
def login(
    payload: users.LoginUserSchema,
    response: Response,
    Authorize: AuthJWT = Depends()
):
    """Login,

    Description:
    
        Allows login

    Args:

        There aren't args

    Returns:
        
        A json with basic user information:

            -id: str
            -email: str
            -name: str
            -age: int
            -token: str

    """
    # Check if the user exist
    if not User.find_one({'email': payload.email.lower()}):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Please verify your email address'
        )
    user = userEntity(
        User.find_one(
            {'email': payload.email.lower()}
        )
    )
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Incorrect Email or Password'
        )

    # Check if user verified his email
    if not user['verified']:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Please verify your email address'
        )

    # Check if the password is valid
    if not utils.verify_password(payload.password, user['password']):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Incorrect Email or Password'
        )

    # Create access token
    access_token = Authorize.create_access_token(
        subject=str(user["id"]),
        expires_time=timedelta(minutes=ACCESS_TOKEN_EXPIRES_IN)
    )

    # Create refresh token
    refresh_token = Authorize.create_refresh_token(
        subject=str(user["id"]),
        expires_time=timedelta(minutes=REFRESH_TOKEN_EXPIRES_IN)
    )

    # Store refresh and access tokens in cookie
    response.set_cookie(
        'access_token',
        access_token,
        ACCESS_TOKEN_EXPIRES_IN * 60,
        ACCESS_TOKEN_EXPIRES_IN * 60,
        '/',
        None,
        False,
        True,
        'lax'
    )
    response.set_cookie(
        'refresh_token',
        refresh_token,
        REFRESH_TOKEN_EXPIRES_IN * 60,
        REFRESH_TOKEN_EXPIRES_IN * 60, '/',
        None,
        False,
        True,
        'lax'
    )
    response.set_cookie(
        'logged_in',
        'True',
        ACCESS_TOKEN_EXPIRES_IN * 60,
        ACCESS_TOKEN_EXPIRES_IN * 60,
        '/',
        None,
        False,
        False,
        'lax'
    )

    # Send both access
    user = userResponseEntity(
        User.find_one(
            {'email': payload.email.lower()}
        )
    )
    user['token'] = access_token
    return user


# @router.get('/refresh')
# def refresh_token(response: Response, Authorize: AuthJWT = Depends()):
#     try:
#         Authorize.jwt_refresh_token_required()

#         user_id = Authorize.get_jwt_subject()
#         if not user_id:
#             raise HTTPException(
#                 status_code=status.HTTP_401_UNAUTHORIZED,
#                 detail='Could not refresh access token'
#             )
#         user = userEntity(
#             User.find_one({'_id': ObjectId(str(user_id))})
#         )
#         if not user:
#             raise HTTPException(
#                 status_code=status.HTTP_401_UNAUTHORIZED,
#                 detail='The user belonging to this token no logger exist'
#             )
#         access_token = Authorize.create_access_token(
#             subject=str(user["id"]), expires_time=timedelta(minutes=ACCESS_TOKEN_EXPIRES_IN)
#         )
#     except Exception as e:
#         error = e.__class__.__name__
#         if error == 'MissingTokenError':
#             raise HTTPException(
#                 status_code=status.HTTP_400_BAD_REQUEST, detail='Please provide refresh token'
#             )
#         raise HTTPException(
#             status_code=status.HTTP_400_BAD_REQUEST, detail=error
#         )

#     response.set_cookie(
#         'access_token',
#         access_token,
#         ACCESS_TOKEN_EXPIRES_IN * 60,
#         ACCESS_TOKEN_EXPIRES_IN * 60,
#         '/',
#         None,
#         False,
#         True,
#         'lax'
#     )
#     response.set_cookie(
#         'logged_in',
#         'True',
#         ACCESS_TOKEN_EXPIRES_IN * 60,
#         ACCESS_TOKEN_EXPIRES_IN * 60,
#         '/',
#         None,
#         False,
#         False,
#         'lax'
#     )
#     return {'access_token': access_token}


@router.get('/logout', status_code=status.HTTP_200_OK)
def logout(
    response: Response,
    Authorize: AuthJWT = Depends(),
    user_id: str = Depends(oauth2.require_user)
):
    """Logout,

    Description:
    
        Allows close the session

    Args:

        There aren't args

    Returns:
        
        A json with basic user information:

            -status: str

    """
    Authorize.unset_jwt_cookies()
    response.set_cookie('logged_in', '', -1)

    return {'status': 'success'}


