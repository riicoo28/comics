from fastapi import APIRouter, Depends, HTTPException, status
from bson.objectid import ObjectId
from serializers.users import userResponseEntity, userListEntity

from config.database import User
from schemas import users
from config import oauth2

router = APIRouter(
    prefix="/users",
    tags=["Users"],
    responses={404: {"description": "Not found"}},
)

@router.get(
    '/me',
    status_code=status.HTTP_200_OK,
    summary="Get my data"
)
def get_me(user_id: str = Depends(oauth2.require_user)):
    """Get my data,

    Description:
    
        Allows to user get his information

    Args:

        There aren't args

    Returns:
        
        A json with basic user information:

            -id: str
            -email: str
            -name: str
            -age: int

    """
    user = userResponseEntity(
        User.find_one({'_id': ObjectId(str(user_id))})
    )
    return user


@router.get(
    "/",
    status_code=status.HTTP_200_OK,
    summary="Find all users"
)
def find_all_users():
    """Find all users,

    Description:
    
        Allows get all users

    Args:

        There aren't args

    Returns:
        
        A array json with basic user information:

            -id: str
            -email: str
            -name: str
            -age: int

    """
    return userListEntity(User.find())

@router.get(
    "/{name}",
    status_code = status.HTTP_200_OK,
    summary="Find users"
)
def find_user(
    name: str,
    token: str = Depends(oauth2.require_user)
):
    """Find users,

    Description:
    
        Allows the search of users by name

    Args:

        name:str: User name to search.

    Returns:
        
        A array json with basic user information:

            -id: str
            -email: str
            -name: str
            -age: int

    """
    if not User.find({"name":{"$regex":name, "$options":"i"}}):
        raise HTTPException(
            status_code = status.HTTP_204_NOT_FOUND,
            detail = "No se encontro ningun usuario"
        )
    user = userListEntity(User.find({"name":{"$regex":name, "$options":"i"}}))
    return user

# @router.put("/{username}")
# def update_user(username: str):
#     return {"username": username}

# @router.delete("/{username}")
# def delete_user(username: str):
#     return {"username": username}
