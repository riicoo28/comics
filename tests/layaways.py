from fastapi import FastAPI
from fastapi.testclient import TestClient
from main import app
from config import oauth2
from fastapi import APIRouter, Response, status, Depends, HTTPException, Body
import json
from config.database import Layaway
from bson.objectid import ObjectId

client = TestClient(app)

def test_get_layaway_list():
    response = client.post("/auth/login", data=json.dumps({
        "email":"user@example.com",
        "password":"rockmax28"        
    }))
    data = {
        "search":"Marvel",
        "order_by":["title"]
    }
    response = client.get("/layaways/getLayawayList", params=data)
    assert response.status_code == 200

def test_make_layaway():
    response = client.post("/auth/login", data=json.dumps({
        "email":"user@example.com",
        "password":"rockmax28"        
    }))
    data = {
        "id_comic":"291",
    }
    response = client.post("/layaways/addToLayaway", params=data)
    Layaway.delete_one({"comic.id":"291"})
    assert response.status_code == 201