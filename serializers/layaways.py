from serializers.users import userSimpleEntity
from serializers.comics import comicEntity

def layawayEntity(layaway) -> dict:
    return {
        "id": str(layaway["_id"]),
        "user": userSimpleEntity(layaway["user"]),
        "comic": comicEntity(layaway["comic"]),
        "created_at": layaway["created_at"],
        "updated_at": layaway["updated_at"]
    }

def createLayawayEntity(layaway) -> list:
    return {
        "user": userSimpleEntity(layaway["user"]),
        "comic": comicEntity(layaway["comic"]),
        "created_at": layaway["created_at"],
        "updated_at": layaway["updated_at"]
    }


def layawaysListEntity(layaways) -> list:
    return [layawayEntity(layaway) for layaway in layaways]