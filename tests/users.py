from fastapi import FastAPI
from fastapi.testclient import TestClient
from main import app
from config import oauth2
from fastapi import APIRouter, Response, status, Depends, HTTPException, Body
import json

client = TestClient(app)

def test_get_me():
    response = client.post("/auth/login", data=json.dumps({
        "email":"user@example.com",
        "password":"rockmax28"        
    }))
    response = client.get("/users/me")
    assert response.status_code == 200

def test_find_all_users():
    response = client.get("/users/")
    assert response.status_code == 200

def test_find_user():
    response = client.get("/users/fer",)
    assert response.status_code == 200