from fastapi import FastAPI
from fastapi.testclient import TestClient
from main import app
from config import oauth2
from fastapi import APIRouter, Response, status, Depends, HTTPException, Body
import json
from config.database import User
from bson.objectid import ObjectId

client = TestClient(app)

def test_create_user():
    data = json.dumps(
        {
            "name": "Prueba",
            "age": 28,
            "email": "prueba@example.com",
            "created_at": "2022-10-25T04:21:09.171995",
            "updated_at": "2022-10-25T05:03:44.636Z",
            "password": "rockmax28",
            "passwordConfirm": "rockmax28",
            "verified": True
        }
    )
    response = client.post("/auth/register", data=data)
    User.delete_one({"_id":ObjectId(response.json()['user']["id"])})
    assert response.status_code == 201

def test_login():
    response = client.post("/auth/login", data=json.dumps({
        "email":"user@example.com",
        "password":"rockmax28"        
    }))
    assert response.status_code == 200

def test_login():
    response = client.post("/auth/login", data=json.dumps({
        "email":"user@example.com",
        "password":"rockmax28"        
    }))
    response = client.get("/auth/logout")
    assert response.status_code == 200