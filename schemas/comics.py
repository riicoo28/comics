from datetime import datetime
from typing import List
from pydantic import BaseModel, Field, EmailStr, constr
from schemas.characters import CharacterBaseSchema

class ComicBaseSchema(BaseModel):
    id:str
    title:str
    image:str
    onSaleDate:datetime | None
    characters:List[CharacterBaseSchema] | None