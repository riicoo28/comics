from fastapi import FastAPI
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

def test_search_comics():
    response = client.get("/comics/searchComics")
    assert response.status_code == 200