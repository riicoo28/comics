# FastApi
from fastapi import APIRouter, Query, status
# Request
import requests
# Comics
from config.config import settings
from serializers import characters
from serializers import comics
from utils import utils



router = APIRouter(
    prefix="/comics",
    tags=["Comics"],
    responses={404: {"description": "Not found"}},
)

@router.get(
    "/searchComics",
    summary="Search comics or characters",
    status_code=status.HTTP_200_OK,
)
def get_all_comics(
    page:int = 1,
    search:str = None,
    filter:str = Query(
        "characters",
        enum=("characters", "comics")
    ),
):
    """Search comics or characters

    Description:
    
        Allows the search of comics and characters in the marvel api

    Args:

        page (int, optional): Number of page in the search. Defaults to 1.
        search (str, optional): Name Character or Title Comic. Defaults to None.
        filter (str, optional): Allows change the search filter between Characters and Comics. Defaults to "characters".

    Returns:

        If filter ir character, returns a json with basic character information:
            -id: str
            -name: str
            -image: str
            -aparences: int

        If filter ir comics, returns a json with basic comic information:
            -id: str
            -title: str
            -image: str
            -onSaleDate: str

    """
    url_marvel = settings.URL_MARVEL    
    limit = 10
    hash_created = utils.create_hash()
    data = {
        "ts":hash_created['timestamp'],
        "apikey":settings.MARVEL_PUBLIC_KEY,
        "hash":hash_created['md5'],
        "limit":limit,
        "offset":(page-1) * limit
    }
    if filter == "characters":
        url_marvel += "characters"
        if search:
            data["nameStartsWith"] = search
    else:
        url_marvel += "comics"
        if search:
            data["titleStartsWith"] = search
    response = requests.get(url_marvel, params=data)
    if filter == "characters":
        response = characters.charactersListEntity(response.json()["data"]["results"])
    else:
        response = comics.comicsListEntity(response.json()["data"]["results"])
    return response