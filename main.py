from fastapi import Depends, FastAPI
from routers import comics, users, auth, layaways

app = FastAPI()

app.include_router(auth.router)
app.include_router(users.router)
app.include_router(comics.router)
app.include_router(layaways.router)
