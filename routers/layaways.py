# Python
import enum
import pymongo
from typing import List, Optional
from bson.objectid import ObjectId
from datetime import datetime
# Request
import requests
# FastApi
from fastapi import APIRouter, Depends, HTTPException, Query, status, Body
# Comics
from serializers.layaways import createLayawayEntity, layawaysListEntity
from serializers.users import userResponseEntity
from config.database import User
from config.database import Layaway
from config.config import settings
from config import oauth2
from utils import utils
from schemas.layaways import LayawayBaseSchema

router = APIRouter(
    prefix="/layaways",
    tags=["Layaway-Comics"],
    responses={404: {"description": "Not found"}},
)

class Order(str, enum.Enum):
    title_descending: str = "title"
    title_ascending: str = "-title"
    onSaleDate_descending: str = "onSaleDate"
    strawberry_ascending: str = "-onSaleDate"

@router.get('/getLayawayList')
def get_layaway_list(
    user_id: str = Depends(oauth2.require_user),
    search: str = Query(default=None),
    order_by:List[Order] = Query(
        example=['title']
    ),
):
    """Get me layaway list,

    Description:
    
        Allows to user show your layaway

    Args:

        search:str: Title comic to search.
        order_by:List[Order]: Args to order the response

    Returns:
        
        A array json with basic layaway information:

            -id: str
            -user: UserBaseSchema
            -comic: ComicBaseSchema
            -created_at: datetime
            -updated_at: datetime

    """
    if order_by:
        sorted = []
    else:
        sorted = [("comic.name", 1)]
    for order in order_by:
        if "-" in order:
            sorted.append(("comic."+order.replace("-",""), pymongo.DESCENDING))
        else:
            sorted.append(("comic."+order, pymongo.ASCENDING))
    filt = {"user.id":user_id}
    if search:
        filt["comic.title"] ={"$regex":search, "$options":"i"}
    layaways = Layaway.find(filt).sort(sorted)
    layaways = layawaysListEntity(layaways)    
    return layaways

@router.post(
    "/addToLayaway",
    response_model=LayawayBaseSchema,
    status_code=status.HTTP_201_CREATED,
)
def make_layaway(
    id_comic:str,
    user_id: str = Depends(oauth2.require_user)
):
    """Add a layaway,

    Description:
    
        Allows to user make a layaway

    Args:

        id_comic:str: Id comic.

    Returns:
        
        A array json with basic layaway information:

            -id: str
            -user: UserBaseSchema
            -comic: ComicBaseSchema
            -created_at: datetime
            -updated_at: datetime

    """
    url_marvel = settings.URL_MARVEL+"/comics/"+id_comic
    hash_created = utils.create_hash()
    data = {
        "ts":hash_created['timestamp'],
        "apikey":settings.MARVEL_PUBLIC_KEY,
        "hash":hash_created['md5'],
    }
    response = requests.get(url_marvel, params=data)
    user = userResponseEntity(User.find_one(
            {'_id': ObjectId(str(user_id))},
        )
    )
    comic = response.json()["data"]["results"][0]
    layaway = createLayawayEntity({
        "user":user,
        "comic":comic,
        "created_at":datetime.utcnow(),
        "updated_at":datetime.utcnow(),
    })
    if Layaway.find_one(
        {"user.id":layaway['user']['id'], "comic.id":layaway['comic']['id']}
    ):
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail='Layaway already exist'
        )
    Layaway.insert_one(layaway)
    # layaway["_id"] = str(layaway["_id"])    
    return layaway