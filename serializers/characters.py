def characterEntity(character) -> dict:
    return {
        "id": str(character["id"]),
        "name": str(character["name"]),
        "image": str(character["thumbnail"]["path"] + '.' + character["thumbnail"]["extension"]),
        "appearances": int(character["comics"]["available"]),
    }

def charactersListEntity(characters) -> list:
    return [characterEntity(character) for character in characters]